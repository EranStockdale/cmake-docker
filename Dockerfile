FROM ubuntu:22.04

# Update and install dependencies
RUN apt update -y
RUN apt install git build-essential libssl-dev -y

# Download, install & build CMake
RUN git clone https://github.com/Kitware/CMake/ /CMake
WORKDIR /CMake
RUN chmod +x ./bootstrap && ./bootstrap
RUN make
RUN make install
WORKDIR /
RUN rm -rf /CMake

ENTRYPOINT [ "/bin/sh" ]